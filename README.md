# Prerequisites and QuickStart Development Process

This repository shows how to setup your local repository for team collaboration and/or quick fixes for salesforce via package structure changes using sfdx-cli force:mdapi commands. It would be an additional exercise to make the repository into a scratch org for development. We make a few assumptions in this README:

- You have [Git](https://git-scm.com/downloads) installed locally with the additional unix commands and are familiar with setting up local and remote repositories.  Here's a link to the [Git docs](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository) if you need a refresher.
- You've succesfully setup the [Salesforce DX CLI](https://trailhead.salesforce.com/en/modules/sfdx_app_dev/units/sfdx_app_dev_setup_dx) and are familiar with [their docs](https://developer.salesforce.com/tools/sfdxcli). 

## QuickSteps

This section assumes you have the prerequisites and access to any necessary repository.

1) Clone the base package repository using the following commands

    > cd c:\working\directory\path
    > git clone https://tjung_seiu-uhw_org@bitbucket.org/tjung_seiu-uhw_org/sfdx-quickstart.git "MyNewProject"
    > cd MyNewProject

# Setup Details


## Setup Working Repository

1) Make sure you have Git installed.  Check by typing `git --version` from the command line and confirm you see the command output in the form of `git version 2.16.2.windows.1`.  If you don't have it installed you can download it [here](https://git-scm.com/downloads).

2) Clone the base package repository using the following commands

    > cd c:\working\directory\path
    > git clone https://<bitbucket_username>@bitbucket.org/seiuuhw_west/sfdx-base-unmanaged-package.git "NewProjectName"
    > cd NewProjectName

3) Remove the current git history

    > rm -rf .git
    
4) Log into Bitbucket Server and [create a new repo](https://confluence.atlassian.com/bitbucketserver/creating-repositories-776639815.html).

5) Locate the new repository url [Source (menu top left) -> Clone (button top right) -> Inside clone button pop up window].  Should be in the format `https://<bitbucket_username>@bitbucket.org/seiuuhw_west/<newreponame>`.

6) Setup new bitbucket repo as new remote repository.

    > git init
    > git add .
    > git commit -m "Initial Commit"
    > git remote add origin <url>
    > git push -u origin master


7) Now you have an empty unmanaged package named `ScratchPushPackage` that can be used to push your local code around salesforce orgs.

8) Make sure you have the Salesforce CLI installed. Check by running `sfdx force --help` and confirm you see the command output. If you don't have it installed you can download and install it from [here](https://developer.salesforce.com/tools/sfdxcli).

9) Setup the full data sandbox with sfdx cli using oAuth.  The following commands will open a browser window for the salesforce.com login.  Inputing your credentials allows sfdx to store an access key locally for interaction with the sandbox of your choice.  (e.g. For setting up our seiu-uhw full sandbox you would want to enter your `username@seiu-uhw.org.uhwtest01` username/password combo when prompted.)

    > sfdx force:auth:web:login -r https://test.salesforce.com -a FullSandbox
    > sfdx force:auth:web:login -r https://test.salesforce.com -a DevSandbox

10) Check the orgs you've logged into with `sfdx force:org:list` to make sure you are connected.  Open the connected org with the alias assigned via the command `sfdx force:org:open -u FullSandBox`

Enjoy!

## Configuration for Salesforce Sandbox Development

Any changes you make to the file directory structure (add/remove of files/folders) need to be properly adjusted within the package.xml file located in the base directory.  Here's the [docs](https://developer.salesforce.com/docs/atlas.en-us.eclipse.meta/eclipse/ide_about_package.htm) and a [sample](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/manifest_samples.htm) of the package.xml file.  This process can be automated by using the `sfdx force:source:convert` command, the Force.com IDE, or the Ant Migration Tool but those methods are beyond the scope of this document.  
- For larger projects it is recommended to use the unmanaged packages to pull down the components required using the `sfdx force:source:retrieve` command.  The process outlined here is for smaller changes.

### Setting up package.xml (with or without destructiveChanges.xml)


1) Once package.xml file is setup properly, push the changes to the sandbox with the following command.

    > sfdx force:mdapi:deploy -d ./mdapi -u FullSandBox -w -1
    > sfdx force:mdapi:deploy -d ./mdapi -u DevSandBox -w -1

2) Congrats!  If no errors then your changes should be pushed to the sandbox.  Now is the perfect time for QA/UAT!


## Contributing to the Repository ###

Please feel free to submit a pull request to make this a better process.


